# Projekt: Analysera annoterade annonser

Syftet är att bygga ett litet verktyg för att analysera de annoteringar som har gjorts och hitta eventuella felaktigheter.

Annoteringarna sparas här: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod

Det finns stor frihet att utföra den här uppgiften som du vill.

## Komma igång

1. Kontrollera att du har [Python3](https://www.python.org/downloads/) installerat. [Poetry](https://python-poetry.org/) är också användbart för att hantera externa bibliotek med mera, men är inget måste.
2. I dagsläget använder vi inga externa bibliotek utöver de bibliotek som skeppas med Python. Men om du börjar använda externa bibliotek behöver du skapa en så kallad *virtual env* innan du börjar jobba med koden: `poetry shell`. Läs mer om det [här](https://python-poetry.org/docs/cli/#shell).
3. Klona *det här repot*: `git clone git@gitlab.com:arbetsformedlingen/taxonomy-dev/backend/nlp/annotated-document-stats.git`
4. Klona repot med annoteringar någonstans: `git clone git@gitlab.com:arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod.git`
5. Uppdatera variabeln `repo_path` i filen `annotation_stats/config.py` att peka på repot som du klonade.
6. Ladda ner filen https://data.jobtechdev.se/taxonomy/begrepp-och-vanliga-relationer.json som innehåller hela taxonomin.
7. Uppdatera variabeln `taxonomy_path` i filen `annotation_stats/config.py` att peka på filen `begrepp-och-vanliga-relationer.json` som du laddade ner.
8. **annotation_stats/demo_annotations.py**: Detta är ett exempelprogram för hur du kan arbeta med annoterade dokument. För att köra det, skriv `python3 annotation_stats/demo_annotations.py` i terminalen.
9. **annotation_stats/demo_taxonomy.py**: Detta är ett exempelprogram som visar hur du kan arbeta med taxonomin. För att köra det, skriv `python3 annotation_stats/demo_taxonomy.py` i terminalen.

## Intressanta data

Här är några saker som skulle vara intressant att veta i de annoterade annonserna:

* Skanna av alla dokument och alla annoteringar i varje dokument och beräkna mängden av alla begrepp som har annoterats. För varje begrepp, lista alla ställen där begreppet förekommer. Finns det begrepp som förekommer flera gånger och i så fall, på vilka olika ställen? 
* Visa ett stapeldiagram över antalet annoteringar för varje *begreppstyp*. Begreppstypen ligger under `type` i annoteringen. Exempelvis har annoteringen `{'preferred-label': 'Deltid', 'type': 'worktime-extent', 'start-position': 30, 'end-position': 36, 'concept-id': '947z_JGS_Uk2', 'matched-string': 'deltid', 'annotation-id': 9}` typen `worktime-extent`.
* Visa en scatter-plot där varje punkt är ett annoterat dokument. Dess x-koordinat är *längden på dokumentets text* (antal tecken), dess y-koordinat är *antalet annoterade ord* i dokumentet. Med hjälp av en sådan plot kan man upptäcka dokument som vi har glömt att annotera.

Du väljer själv hur du presenterar ovanstående. Du kan t.ex. skriva separata skript för varje punkt. Du kan också hitta på andra saker som du vill analysera.
