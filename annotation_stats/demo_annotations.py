import importlib
import common; importlib.reload(common)
import config; importlib.reload(config)

## Load the documents and the taxonomy

taxonomy = common.load_taxonomy(config.taxonomy_path)
document_dict = common.load_annotated_documents(config.repo_path)

print("Loaded {:d} annotated documents".format(len(document_dict)))

## Show some information about a document and an annotation in that document

first_doc = list(document_dict.values())[0]
text = first_doc["text"]
annotations = first_doc["annotations"]

print("The first document contains this text: '{:s}'\n".format(common.abbreviate(text, 60)))
print("It contains {:d} annotations\n".format(len(annotations)))
print("The first annotation has the following information")
annotation = annotations[0]
print("  Annotated string: {:s}".format(annotation["matched-string"]))
print("  Concept id: {:s}".format(annotation["concept-id"]))
print("  Concept type: {:s}".format(annotation["type"]))
print("\nRaw data")
print(annotation)
print("\nMore information about the annotated concept")
print(taxonomy[annotation["concept-id"]])
