import json
import os

def read_json(filename):
    """Read JSON data from a file and return the decoded datastructure"""
    with open(filename, "r") as f:
        return json.load(f)
    
def load_annotated_documents(path):
    """Scan the repository at `path` and load all files with annotated documents into a dict"""
    all_files = os.listdir(path)
    dst = {}
    for filename in all_files:
        if filename.lower().endswith(".json"):
            data = read_json(os.path.join(path, filename))
            sha1 = data.get("sha1")
            if sha1 != None:
                dst[sha1] = data
    return dst

def load_taxonomy(filename):
    """"Load all taxonomy concepts from a file and return a dict"""
    raw_data = read_json(filename)
    concepts = raw_data["data"]["concepts"]
    return {concept["id"]:concept for concept in concepts}

def abbreviate(text, maxlen):
    n = len(text)
    if n <= maxlen:
        return text

    return text[0:maxlen-3] + "..."
