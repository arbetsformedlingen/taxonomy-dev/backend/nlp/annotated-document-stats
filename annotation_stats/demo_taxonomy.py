import common
import config

the_taxonomy = common.load_taxonomy(config.taxonomy_path)

def pretty_print_concept(concept):
    """Display information about a concept in a human readable way."""
    print("*** Concept {:s} of type '{:s}'".format(concept["id"], concept["type"]))
    print("  Preferred label: '{:s}'".format(concept["preferred_label"]))
    altlabels = concept["alternative_labels"]
    if 0 < len(altlabels):
        print("  Alternative labels:")
        for label in altlabels:
            print("  * '{:s}'".format(label))

def broader_sort_order(concept):
    t = concept["type"]
    if "isco" in t or "esco" in t:
        return 10
    if t == "occupation-group":
        return 5
    return 0
    
def get_broader_jobtech_concepts(taxonomy, concept):
    """Return the broader concepts. Normally, a concept should have at most one broader concept but that is not always true in the taxonomy. In case that happens, the broader concepts are sorted by relevance."""
    broader = [taxonomy[b["id"]] for b in concept["broader"]]
    return list(sorted(broader, key=broader_sort_order))
            
def follow_broader(taxonomy, concept):
    """Follows the broader relation up through the tree."""
    result = []
    while True:
        # Lookup *all* data of the concept
        concept = taxonomy[concept["id"]]
        result.append(concept)        
        broader = get_broader_jobtech_concepts(taxonomy, concept)
        if 0 < len(broader):
            if len(broader) != 1:
                print("Warning: Multiple broader concepts at {:s}".format(concept["id"]))
            concept = broader[0]
        else:
            break
    return result

sample_concept_id = "QvzY_zbq_Bb8"

# Get information about a concept given the id
concept = the_taxonomy[sample_concept_id]

# Get a list of this concept and its ancestors by following the 'broader' relation.
concept_and_ancestors = follow_broader(the_taxonomy, concept)

print("Path of concepts by following the broader relation:")
# Pretty print this concept and its ancestors
for c in concept_and_ancestors:
    pretty_print_concept(c)
                
